#!/bin/bash

PS3=$'\e[1;32mO que deseja fazer?\033[0m: '
options=(
	"Configurar Laravel"
	"Configurar NodeJS"
	"Configurar ReactJS"
  "Configurar VueJS"
	"Remover server blocks"
	"Voltar"
)

COLUMNS=6
select opt in "${options[@]}";
do
	case $REPLY in
		1) /root/automation/serverblocks/laravel/installer.sh; break ;;
		2) /root/automation/serverblocks/nodejs/installer.sh; break ;;
		3) /root/automation/serverblocks/reactjs/installer.sh; break ;;
    4) /root/automation/serverblocks/vuejs/installer.sh; break ;;
		5) /root/automation/serverblocks/remove.sh; break ;;
		6) /root/automation/installer.sh; break ;;
	esac
done
