#!/bin/bash

domain(){
	echo -e '\033[1;96m########## SUBDOMÍNIO ##########\033[0m'
	pergunta=(
		"Digite o nome do server block: "
		"Digite o nome do domínio(sem www): "
		"Digite a porta: "
	)

	for((contador=0;contador<3;contador++)); do
		if [ -z ${resposta[$contador]} ]; then
			read -p $"${pergunta[$contador]}" resposta[$contador]
			let contador--;
		fi
	done

	sudo mkdir -p /var/www/${resposta[0]}
	sudo mkdir -p /var/log/nginx/
	sudo mkdir -p /var/log/nginx/${resposta[0]}/

	service nginx stop

	URI='$uri'
	HOST='$host'
	REQUEST_URI='$request_uri'
	QUERY_STRING='$query_string'
	DOCUMENT_ROOT='$document_root'
	FASTCGI_SCRIPT_NAME='$fastcgi_script_name'
	HTTP_UPGRADE='$http_upgrade'

  sudo certbot --nginx -d ${resposta[1]}
	cd /etc/nginx/sites-available/

cat <<EOT >> ${resposta[0]}
server {
    listen 443 ssl;

    root /var/www/${resposta[0]};
    index index.php index.html index.htm;

    server_name ${resposta[1]};

    access_log /var/log/nginx/${resposta[0]}/localhost_access.log;
    error_log /var/log/nginx/${resposta[0]}/localhost_error.log;
    rewrite_log on;

    ssl_certificate /etc/letsencrypt/live/${resposta[1]}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/${resposta[1]}/privkey.pem;

	location /  {
		proxy_pass http://127.0.0.1::${resposta[2]};
		proxy_http_version 1.1;
		proxy_set_header Upgrade $HTTP_UPGRADE;
		proxy_set_header Connection 'upgrade';
		proxy_set_header Host $HOST;
		proxy_cache_bypass $HTTP_UPGRADE;
	}
}

server {
    listen 80;
    server_name ${resposta[1]};
    return 301 https://$HOST$REQUEST_URI;
}
EOT

	sudo ln -s /etc/nginx/sites-available/${resposta[0]} /etc/nginx/sites-enabled/
	sudo chown -R www-data:www-data /var/www/${resposta[0]}
	sudo chmod 755 /var/www/${resposta[0]}
  sudo fuser -k 80/tcp
	sudo systemctl restart nginx
}

domain
