#!/bin/bash

PS3=$'\e[1;32mO que deseja fazer?\033[0m: '
options=(
	"Configurar domínio"
	"Configurar subdomínio"
	"Voltar"
)

COLUMNS=3
select opt in "${options[@]}";
do
	case $REPLY in
		1) /root/automation/serverblocks/nodejs/domain.sh; break ;;
		2) /root/automation/serverblocks/nodejs/subdomain.sh; break ;;
		3) /root/automation/serverblocks/installer.sh; break ;;
	esac
done
