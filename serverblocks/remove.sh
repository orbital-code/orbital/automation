#!/bin/bash

remove(){
	echo -e '\033[1;96m########## REMOVER SERVER BLOCK ##########\033[0m'
	pergunta=(
		"Digite o nome do server block: "
		"Digite o nome do domínio(sem www): "
	)
	for((contador=0;contador<2;contador++)); do
		if [ -z ${resposta[$contador]} ]; then
			read -p $"${pergunta[$contador]}" resposta[$contador]
			let contador--;
		fi
	done

	sudo rm -rf /var/log/nginx/${resposta[0]}/
	sudo rm -rf /var/www/${resposta[0]}/
	sudo rm -rf /etc/nginx/sites-enabled/${resposta[0]}
	sudo rm -rf /etc/nginx/sites-available/${resposta[0]}

	rm -rf /etc/letsencrypt/live/${resposta[1]}
	rm -rf /etc/letsencrypt/renewal/${resposta[1]}.conf
	rm -rf /etc/letsencrypt/archive/${resposta[1]}
}

remove
