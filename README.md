# Orbital Bash

  ### Instalação

  - Acesse a raiz do servidor
  - Faz um clone do projeto **git clone git@gitlab.com:orbital-code/orbital/automation.git**
  - Rode o seguinte comando

```sh  
 find ~/automation/ -type f -iname "*.sh" -exec chmod +x {} \;
 ln -s ~/automation/installer.sh /usr/local/bin/workspace
```

 Após isso digite no terminal o seguinte comando: ***worksapce***
 