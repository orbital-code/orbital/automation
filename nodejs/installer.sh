#!/bin/bash

PS3=$'\e[1;32mO que deseja fazer?\033[0m: '
options=(
	"Instalar nvm"
	"Instalar yarn"
	"Instalar pm2"
	"Voltar"
)

COLUMNS=4
select opt in "${options[@]}";
do
	case $REPLY in
		1) /root/automation/nodejs/nvm.sh; break ;;
		2) /root/automation/nodejs/yarn.sh; break ;;
		3) /root/automation/nodejs/pm2.sh; break ;;
		4) /root/automation/installer.sh; break ;;
	esac
done
