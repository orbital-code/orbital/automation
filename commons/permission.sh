#!/bin/bash

permission(){
	echo -e '\033[1;96m########## PERMISSÃO DE PASTA ##########\033[0m'
	pergunta=(
		"Digite o nome do usuário: "
		"Digite o virtual server que o usuário terá acesso: "
	)
	for((contador=0;contador<2;contador++)); do
		if [ -z ${resposta[$contador]} ]; then
			read -p $"${pergunta[$contador]}" resposta[$contador]
			let contador--;
		fi
	done

	sudo chown ${resposta[0]}:${resposta[0]} /var/www/${resposta[1]}
	sudo chmod 700 /var/www/${resposta[1]}
	sudo systemctl restart nginx
}

permission
