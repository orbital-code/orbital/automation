#!/bin/bash

letsencrypt(){
	echo -e '\033[1;96m########## LETSENCRYPT ##########\033[0m'
	sudo apt install certbot python3-certbot-nginx
	sudo systemctl reload nginx
}

letsencrypt