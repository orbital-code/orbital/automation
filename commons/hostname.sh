#!/bin/bash

hostname(){
	echo -e '\033[1;96m########## HOSTNAME ##########\033[0m'
	hostnamectl
	hostnamectl set-hostname localhost
	hostnamectl
}

hostname