#!/bin/bash

reload(){
	echo -e '\033[1;96m########## RECARREGAR ##########\033[0m'
  sudo fuser -k 80/tcp
	sudo chown -R www-data:www-data /var/www
	sudo chmod 755 /var/www
	sudo systemctl restart nginx
}

reload
