#!/bin/bash

composer(){
	echo -e '\033[1;96m########## COMPOSER ##########\033[0m'
	curl -sS https://getcomposer.org/installer | /usr/bin/php
	sudo mv composer.phar /usr/local/bin/composer
}

composer