#!/bin/bash

remove(){
	echo -e '\033[1;96m########## REMOVER NGINX ##########\033[0m'
  sudo fuser -k 80/tcp
	rm -rf /etc/nginx/sites-available/default
	rm -rf /etc/nginx/sites-enabled/default
}

remove
