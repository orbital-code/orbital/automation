#!/bin/bash

PS3=$'\e[1;32mO que deseja fazer?\033[0m: '
options=(
	"Instalar PHP 7.2"
	"Instalar PHP 7.3"
	"Instalar PHP 7.4"
	"Voltar"
)

COLUMNS=4
select opt in "${options[@]}";
do
	case $REPLY in
		1) /root/automation/php/php7.2.sh; break ;;
		2) /root/automation/php/php7.3.sh; break ;;
		3) /root/automation/php/php7.4.sh; break ;;
		4) /root/automation/installer.sh; break ;;
	esac
done
