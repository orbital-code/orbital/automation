#!/bin/bash

php(){
	echo -e '\033[1;96m########## PHP ##########\033[0m'
	sudo sudo add-apt-repository -y ppa:ondrej/php && sudo apt-get update
	sudo apt-get install php7.4-cli php7.4-fpm php7.4-mysql php7.4-curl php7.4-dev
	sudo apt-get install php7.4-mbstring php7.4-gd php-imagick php7.4-json php7.4-xml
	sudo apt-get install php7.4-zip php7.4-mysql php7.4-odbc php7.4-soap git
	sudo apt-get install bc mailutils supervisor php7.4-imap php-uuid
	sudo apt-get install php-memcache php-memcached php-redis php-mongodb php7.4-sqlite3
	sudo apt-get install php-redis apache2-utils
	apt-get install mysql-client redis-server
	sudo systemctl restart nginx
}

php
