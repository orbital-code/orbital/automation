#!/bin/bash

php(){
	echo -e '\033[1;96m########## PHP ##########\033[0m'
	sudo sudo add-apt-repository -y ppa:ondrej/php && sudo apt-get update
	sudo apt-get install php7.3-cli php7.3-fpm php7.3-mysql php7.3-curl php7.3-dev
	sudo apt-get install php7.3-mbstring php7.3-gd php-imagick php7.3-json php7.3-xml
	sudo apt-get install php7.3-zip php7.3-mysql php7.3-odbc php7.3-soap git
	sudo apt-get install bc mailutils supervisor php7.3-imap php-uuid
	sudo apt-get install php-memcache php-memcached php-redis php-mongodb php7.3-sqlite3
	sudo apt-get install php-redis apache2-utils
	apt-get install mysql-client redis-server
	sudo systemctl restart nginx
}

php
