#!/bin/bash

php(){
	echo -e '\033[1;96m########## PHP ##########\033[0m'
	sudo sudo add-apt-repository -y ppa:ondrej/php && sudo apt-get update
	sudo apt-get install php7.2-cli php7.2-fpm php7.2-mysql php7.2-curl php7.2-dev
	sudo apt-get install php7.2-mbstring php7.2-gd php-imagick php7.2-json php7.2-xml
	sudo apt-get install php7.2-zip php7.2-mysql php7.2-odbc php7.2-soap git
	sudo apt-get install bc mailutils supervisor php7.2-imap php-uuid
	sudo apt-get install php-memcache php-memcached php-redis php-mongodb php7.2-sqlite3
	sudo apt-get install php-redis apache2-utils
	apt-get install mysql-client redis-server
	sudo systemctl restart nginx
}

php