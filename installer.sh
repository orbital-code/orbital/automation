#!/bin/bash

PS3=$'\e[1;32mO que deseja fazer?\033[0m: '
options=(
	"Setar hostname"
	"Instalar nginx"
  "Remover nginx default"
  "Instalar php"
  "Instalar composer"
  "Configurar timezone"
	"Instalar letsencrypt"
	"Instalar banco de dados"
	"Instalar nodejs"
	"Server blocks"
	"Deploy"
	"Recarregar servidor"
	"Atualizar servidor"
	"Adicionar usuário"
	"Permissão de pasta"
	"Sair"
)

COLUMNS=16
select opt in "${options[@]}";
do
	case $REPLY in
		1) /root/automation/commons/hostname.sh; break ;;
		2) /root/automation/commons/nginx.sh; break ;;
		3) /root/automation/commons/remove.sh; break ;;
		4) /root/automation/php/installer.sh; break ;;
		5) /root/automation/commons/composer.sh; break ;;
		6) /root/automation/commons/timezone.sh; break ;;
		7) /root/automation/commons/letsencrypt.sh; break ;;
		8) /root/automation/database/installer.sh; break ;;
		9) /root/automation/nodejs/installer.sh; break ;;
		10) /root/automation/serverblocks/installer.sh; break ;;
		11) /root/automation/deploy/installer.sh; break ;;
		12) /root/automation/commons/restart.sh; break ;;
    13) /root/automation/commons/upgrade.sh; break ;;
    14) /root/automation/commons/users.sh; break ;;
    15) /root/automation/commons/permission.sh; break ;;
    16) break ;;
	esac
done
