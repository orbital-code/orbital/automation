#!/bin/bash

mysql(){
	echo -e '\033[1;96m########## MYSQL ##########\033[0m'
  sudo apt update
  sudo apt install mysql-server
  sudo mysql_secure_installation
  systemctl status mysql.service
}

mysql
