#!/bin/bash

PS3=$'\e[1;32mO que deseja fazer?\033[0m: '
options=(
	"Instalar MongoDB"
	"Instalar MySQL"
	"Voltar"
)

COLUMNS=3
select opt in "${options[@]}";
do
	case $REPLY in
		1) /root/automation/database/mongodb.sh; break ;;
		2) /root/automation/database/mysql.sh; break ;;
		3) /root/automation/installer.sh; break ;;
	esac
done
