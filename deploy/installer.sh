#!/bin/bash

PS3=$'\e[1;32mO que deseja fazer?\033[0m: '
options=(
	"Laravel"
	"NodeJs"
	"ReactJS"
  "VueJS"
	"Remover"
	"Voltar"
)

COLUMNS=6
select opt in "${options[@]}";
do
	case $REPLY in
		1) /root/automation/deploy/laravel.sh; break ;;
		2) /root/automation/deploy/nodejs.sh; break ;;
		3) /root/automation/deploy/reactjs.sh; break ;;
    4) /root/automation/deploy/vuejs.sh; break ;;
		5) /root/automation/deploy/remove.sh; break ;;
		6) /root/automation/installer.sh; break ;;
	esac
done
