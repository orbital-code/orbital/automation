#!/bin/bash

deploy(){
	echo -e '\033[1;96m########## DEPLOY ##########\033[0m'
	pergunta=(
		"Digite o nome do server block: "
		"Digite o nome do repositótio: "
	)
	for((contador=0;contador<2;contador++)); do
		if [ -z ${resposta[$contador]} ]; then
			read -p $"${pergunta[$contador]}" resposta[$contador]
			let contador--;
		fi
	done

	REF_NAME='$ref'

	mkdir -p /git/${resposta[1]}.git
	cd /git/${resposta[1]}.git
	git init --bare
	cd hooks

cat <<EOT >> post-receive
#!/bin/bash
while read oldrev newrev ref
do
  if [[ $REF_NAME =~ .*/master$ ]];
  then
    GIT_WORK_TREE=/var/www/${resposta[0]} git checkout -f master
    echo "changes pushed to master"
  fi

  if [[ $REF_NAME =~ .*/develop$ ]];
  then
    GIT_WORK_TREE=/var/www/${resposta[0]} git checkout -f  develop
    echo "changes pushed to develop"
  fi

  if [[ $REF_NAME =~ .*/maintenance$ ]];
  then
    GIT_WORK_TREE=/var/www/${resposta[0]} git checkout -f  maintenance
    echo "changes pushed to maintenance"
  fi
done

cd /var/www/${resposta[0]}
mv .env.production .env
sudo npm install && npm run build
GLOBIGNORE=build
sudo rm -rf *
unset GLOBIGNORE
EOT

	chmod +x post-receive
	sudo systemctl restart nginx
}

deploy
